const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.post('/add', (request, response) => {
  const { name, salary, age } = request.body

  const query = `
    INSERT INTO employee
      (name, salary, age)
    VALUES
      ('${name}',${salary},${age})
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.get('/display', (request, response) => {
  const query = `
    SELECT empid, name, salary, age
    FROM employee
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.put('/update/:id', (request, response) => {
  const { id } = request.params
  const { salary, age } = request.body

  const query = `
    UPDATE employee
    SET
      salary = ${salary}, 
      age = ${age}
    WHERE
      empid = ${id}
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.delete('/delete/:id', (request, response) => {
  const { id } = request.params

  const query = `
    DELETE FROM employee
    WHERE
      empid = ${id}
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

module.exports = router